//
//  ViewController.swift
//  nsuser-defaults
//
//  Created by iulian david on 11/11/16.
//  Copyright © 2016 iulian david. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var favColor: UILabel!
    
    var people = [Person]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let color = UserDefaults.standard.value(forKey: "color") {
             favColor.text = "Favorite Color: \(color)"
        } else {
            favColor.text = "Pick a color"
        }
        
        let personA = Person(firstName: "iuli", lastName: "david")
        let personB = Person(firstName: "alex", lastName: "david")
        let personC = Person(firstName: "muffin", lastName: "david")
        
        people.append(personA)
        people.append(personB)
        people.append(personC)
        
        //Trebuie setat NsKeyedArchiver
        let peopleData = NSKeyedArchiver.archivedData(withRootObject: people)
        //asta se stocheaza serializat
        UserDefaults.standard.set(peopleData, forKey: "people")
        UserDefaults.standard.synchronize()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func changeFavoriteColor(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            favColor.text = "Favorite Color: Red"
            UserDefaults.standard.set("Red", forKey: "color")
            UserDefaults.standard.synchronize()
            
        case 1:
            favColor.text = "Favorite Color: Yellow"
            UserDefaults.standard.set("Yellow", forKey: "color")
            UserDefaults.standard.synchronize()
        case 2:
            favColor.text = "Favorite Color: Green"
            UserDefaults.standard.set("Green", forKey: "color")
            UserDefaults.standard.synchronize()
            loadNsUserData()
        default:
            favColor.text = "Favorite Color"
        }
        
    }
    
    
    func loadNsUserData(){
        //se ia data din UserDefaults
        if let loadedPeople = UserDefaults.standard.data(forKey: "people")! as? Data {
            //se dezarhiveaza
            if let peopleArray = NSKeyedUnarchiver.unarchiveObject(with: loadedPeople) as? [Person] {
                for person in peopleArray {
                    print(person.firstName)
                }
            }
        }
            
    }
}

