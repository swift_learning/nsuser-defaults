//
//  Person.swift
//  nsuser-defaults
//
//  Created by iulian david on 11/11/16.
//  Copyright © 2016 iulian david. All rights reserved.
//

import UIKit



class Person: NSObject, NSCoding {
    
    private final var _firstName: String!
    
    private final var _lastName: String!
    
    init(firstName: String, lastName: String) {
        _firstName = firstName
        _lastName = lastName
    }
    
    var firstName: String {
        get{
            return _firstName
        }
    }
    
    var lastName: String {
        get{
            return _lastName
        }
    }
    
    override init(){
        
    }
    required convenience init?(coder aDecoder: NSCoder){
        self.init()
        self._firstName = (aDecoder.decodeObject(forKey: "firstName") as! String)
        self._lastName = (aDecoder.decodeObject(forKey: "lastName") as! String)
    }
    
    func  encode(with aCoder: NSCoder) {
        aCoder.encode(self._firstName, forKey: "firstName")
        aCoder.encode(self._lastName, forKey: "lastName")
    }
}
